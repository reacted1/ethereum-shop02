import AppRoutes from './routes'
import {BtnProvider} from './context/context.jsx'
import './App.scss'

function App() {
    return (
        <>
            <BtnProvider>
                <AppRoutes />
            </BtnProvider>
        </>
  )
}

export default App
