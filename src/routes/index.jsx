import {Routes, Route} from "react-router-dom";
import HomePage from "../pages/HomePage.jsx";
import FavoritePage from "../pages/FavoritePage/FavoritePage.jsx";
import CartPage from "../pages/CartPage/CartPage.jsx";

export default () => {
    return (
        <Routes>
            {/*<Route path={"/"} element={<HomePage onFavorite={onFavorite} favorites={favorites}/>}/>*/}
            <Route path={"/"} element={<HomePage/>}/>
            <Route path={"/favorite"} element={<FavoritePage />}/>
            <Route path={"/cart"} element={<CartPage />}/>
        </Routes>
    )
}