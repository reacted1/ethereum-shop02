import { configureStore } from '@reduxjs/toolkit'
import favoriteSlice from "./slices/favorite.js";
import nftsSlice from './slices/nfts.js';
import cartSlice from './slices/cartList.js'
export default configureStore({
    reducer: {
        nfts: nftsSlice,
        favorite: favoriteSlice,
        cart: cartSlice,
    }
})