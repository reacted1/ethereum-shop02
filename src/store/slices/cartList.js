import { createSlice } from '@reduxjs/toolkit'

const initialState = {
    cartList: [],
}

const cartSlice = createSlice({
    name: 'cart',
    initialState,
    reducers: {
        cartListAdd: (state, {payload}) => {
            const isItemExist = state.cartList.some(item => item.id === payload.id);
            if (!isItemExist) {
                state.cartList = [...state.cartList, payload];
            }
        },
        cartListDelete: (state, { payload }) => {
            state.cartList = state.cartList.filter( item => item.id !== payload );
        }
    }
})
export const { cartListDelete, cartListAdd } = cartSlice.actions
export default cartSlice.reducer;