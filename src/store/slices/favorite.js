import { createSlice } from '@reduxjs/toolkit'

const initialState = {
   favoriteList: [],
}

const favoriteSlice = createSlice({
    name: 'favorite',
    initialState,
    reducers: {
        favListAdd: (state, { payload }) => {
            const isItemExist = state.favoriteList.some(item => item.id === payload.id);
            if (!isItemExist) {
                state.favoriteList = [...state.favoriteList, payload];
            }
        },
        favListDelete: (state, { payload }) => {
            state.favoriteList = state.favoriteList.filter( item => item.id !== payload );
        }
    }
})

export const { favListAdd, favListDelete } = favoriteSlice.actions
export default favoriteSlice.reducer;