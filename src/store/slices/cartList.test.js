import {cartListAdd, cartListDelete} from "./cartList.js";
import cartSlice from './cartList.js'
describe('cartList reducer', () => {
    it('have to check action cartListAdd', () => {
        const initialState = {
            cartList: [],
        };
        const payload = { id: 0, name: 'home' };
        const nextState = cartSlice(initialState, cartListAdd(payload));

        expect(nextState.cartList.length).toEqual(1);
        expect(nextState.cartList[0]).toEqual(payload);
    });

    it('have to handle cartListDelete', () => {
        // Set initial state with some items
        const initialState = {
            cartList: [
                { id: 5, name: 'fffffff' },
                { id: 3, name: 'bbbbbbb' },
                { id: 11, name: 'idid' },
                { id: 7, name: '938djhd' },
            ],
        };
        const payload = 3;

        const nextState = cartSlice(initialState, cartListDelete(payload));

        expect(nextState.cartList.length).toEqual(3);
        expect(nextState.cartList.map(item => item.id)).not.toContain(payload);
    });
});