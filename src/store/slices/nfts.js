import { createSlice, createAsyncThunk } from '@reduxjs/toolkit'
import {sendRequest} from "../../helpers/sendRequest.js";
import {API_URL} from "../../constants/API.js";

const initialState = {
    nftsList: [],
}

export const actionFetchNfts = createAsyncThunk(
    'nfts/fetchNfts',
    async () => {
        const data = await sendRequest(`${API_URL}`);
        return data;
    }
)

const nftsSlice = createSlice({
    name: "nfts",
    initialState,
    reducers: {},
    extraReducers: (builder) => {
        builder
            .addCase(actionFetchNfts.fulfilled, (state, {payload}) => {
                if (payload?.length > 0) {
                    state.nftsList = [...payload];
                }
            })
    }
})

export default nftsSlice.reducer