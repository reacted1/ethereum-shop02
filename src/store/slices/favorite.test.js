import { favListAdd, favListDelete } from './favorite.js';
import favoriteSlice from "./favorite.js";
describe('favorite reducer', () => {
    it('have to check action favListAdd', () => {
        const initialState = {
            favoriteList: [],
        };
        const payload = { id: 1, name: 'djfbndjfbdfjdhb' };

        const nextState = favoriteSlice(initialState, favListAdd(payload));

        expect(nextState.favoriteList.length).toEqual(1);
        expect(nextState.favoriteList[0]).toEqual(payload);
    });

    it('have to handle favListDelete', () => {
        // Set initial state with some items
        const initialState = {
            favoriteList: [
                { id: 1, name: 'test 21' },
                { id: 2, name: 'test 22' },
            ],
        };
        const payload = 1;

        const nextState = favoriteSlice(initialState, favListDelete(payload));

        expect(nextState.favoriteList.length).toEqual(1);
        expect(nextState.favoriteList.map(item => item.id)).not.toContain(payload);
    });
});