import Header from "../components/Header/Header.jsx";
import CardsList from "../components/ShopItems/CardsList/CardsList.jsx";
import Footer from "../components/Footer/Footer.jsx";

const HomePage = () => {
    return (
        <>
            <Header/>
            <div className="main-content g-container">
                <CardsList />
            </div>
            <Footer/>
        </>
    )
}

export default HomePage;