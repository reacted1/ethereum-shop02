import * as React from 'react';
import Button from '@mui/material/Button';
import {useSelector} from "react-redux";
import {Formik, Form, Field} from "formik";
import {CheckoutFormSchema} from "../../helpers/validation.js";

import './CartForm.scss'

export default function MultilineTextFields() {
    const cartList = useSelector((state)=> state.cart.cartList);

    const calcOrderSum = () => {
        let sum = 0;
        cartList.map((el) => {
            sum += +el.price;
        })
        return sum;
    }

    return (
        <Formik
            initialValues={{
                firstName: '',
                lastName: '',
                age: 0,
                phone: "",
                address: "",
                comment: "",
            }}
            validationSchema={CheckoutFormSchema}
            onSubmit={(values) => console.log(values)}
        >
            {({errors, touched}) => (
                <Form className={"cart-form"}>
                    <div>
                        <Field
                            className="nice-form-group"
                            name={"firstName"}
                            id="outlined-textarea"
                            placeholder="Your name"
                        />
                        {errors.firstName && touched.firstName ? (
                            <div className={"error-message"}>{errors.firstName}</div>
                        ) : null}
                        <Field
                            className="nice-form-group"
                            name={"lastName"}
                            id="outlined-textarea"
                            placeholder="Your lastname"
                        />
                        {errors.lastName && touched.lastName ? (
                            <div className={"error-message"}>{errors.lastName}</div>
                        ) : null}
                    </div>
                    <div>
                        <Field
                            className="nice-form-group"
                            name={"age"}
                            id="outlined-textarea"
                            placeholder="Your age"
                        />
                        {errors.age && touched.age ? (
                            <div className={"error-message"}>{errors.age}</div>
                        ) : null}
                        <Field
                            className="nice-form-group"
                            name={"phone"}
                            id="outlined-textarea"
                            placeholder="Phone number"
                        />
                        {errors.phone && touched.phone ? (
                            <div className={"error-message"}>{errors.phone}</div>
                        ) : null}
                    </div>
                    <div>
                        <Field
                            className="nice-form-group"
                            name={"address"}
                            id="outlined-textarea"
                            placeholder="Address"
                        />
                        {errors.address && touched.address ? (
                            <div className={"error-message"}>{errors.address}</div>
                        ) : null}
                        <Field
                            className="nice-form-group"
                            name={"comment"}
                            id="outlined-multiline-static"
                            placeholder={"(Optional)"}
                        />
                        {errors.comment && touched.comment ? (
                            <div className={"error-message"}>{errors.comment}</div>
                        ) : null}
                    </div>
                    <p className={"form-oder-sum"}>Order sum:  {calcOrderSum()} ETH</p>
                    <p className={"form-oder-sum_usd"}>Total order in USD: ${calcOrderSum()*3300}</p>
                    <Button type={"submit"} variant="contained">Checkout</Button>
                </Form>
            )}
        </Formik>
    );
}