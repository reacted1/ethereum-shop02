import Header from "../../components/Header/Header.jsx";
import {useDispatch, useSelector} from "react-redux";
import {cartListDelete} from "../../store/slices/cartList.js";
import MultilineTextFields from "./CartForm.jsx";
import '../Pages.scss'
import './CartPage.scss'
import ItemInCard from "./ItemInCard.jsx";


const CartPage = () => {
    const cartList = useSelector((state) => state.cart.cartList)
    const dispatch = useDispatch();
    return (
        <div>
            <Header />
            <div className="main-content g-container">
                {(cartList.length > 0) ? (
                    <div className="checkout-main">
                        <div className="checkout-form">
                            <MultilineTextFields/>
                        </div>
                        <div className="checkout-list">
                            { cartList.map((item)=> (
                                    <ItemInCard
                                        key={item.id}
                                        image_path={item.image_path}
                                        title={item.title}
                                        price={item.price}
                                        onClick={() => {dispatch(cartListDelete(item.id))}}
                                    />
                                )
                            )}
                        </div>
                    </div>
                    )
                    : (
                        <div className="no-items_box">
                            <h3 className={"no-items_box__text"}>No items added yet</h3>
                        </div>
                    )}
            </div>
        </div>
    )
}

export default CartPage;