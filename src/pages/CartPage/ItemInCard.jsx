import React, { useContext } from 'react';
import PropTypes from "prop-types";
import Button from "../../components/Button/Button.jsx";
import {IMG_URL} from "../../constants/API.js";
import {SaveContext} from "../../context/context.jsx";

const ItemInCard = ({id, image_path, title, price, onClick, onCart}) => {
    const pathName = window.location.pathname;
    const { savedItems } = useContext(SaveContext);
    const { inCart, toCartItem } = useContext(SaveContext);
    const isInCart = inCart.includes(id);
    const isSaved = savedItems.includes(id);

    return (
        <div className={`card`}>
            <div className={`cardItem`}>
                <div className={`image-box`}>
                    <img src={`${IMG_URL}${image_path}`} alt={title}/>
                </div>
                <div className={`info-box`}>
                    <p className={`info-box_title`}>
                        {title}
                    </p>
                    <p className={`info-box_price`}>
                        {price} ETH ( ${price*3100} )
                    </p>
                </div>
                {(pathName==="/") ? (
                    <div className={`btn-box`}>
                        <Button className={`btn btn-item btn-left`} type="button" onClick={onClick}>
                            {isSaved ? 'Saved' : 'Save'}
                        </Button>
                        <Button className={`btn btn-item btn-right`} type="button" onClick={() => {
                            onCart();
                            toCartItem(id)
                        }}>
                            {isInCart ? "Remove Cart" : "Add to Cart"}
                        </Button>
                    </div>) : (
                    <div className={`btn-box`}>
                        <Button className={"btn btn-item btn-remove"} type="button" onClick={onClick}>Remove from list</Button>
                    </div>
                )}
            </div>
        </div>
    )
}

ItemInCard.propTypes = {
    image_path: PropTypes.string,
    title: PropTypes.string,
    id: PropTypes.string,
    price: PropTypes.string,
    onClick: PropTypes.func,
    onCart: PropTypes.func,
    restProps: PropTypes.any,
}

export default ItemInCard;