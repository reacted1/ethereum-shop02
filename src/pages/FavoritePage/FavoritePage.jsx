import Header from "../../components/Header/Header.jsx";
import {useDispatch, useSelector} from "react-redux";
import CardItem from "../../components/ShopItems/CardsList/CardItem.jsx";
import {SaveContext} from "../../context/context.jsx";
import {favListDelete} from "../../store/slices/favorite.js";
import '../Pages.scss'
import {useContext} from "react";

const FavoritePage = () => {
    const favArr = useSelector((state) => state.favorite.favoriteList)
    const dispatch = useDispatch();
    const { themeClass } = useContext(SaveContext);
    return (
        <div>
            <Header />
            <div className="main-content g-container">
                {(favArr.length > 0) ? (
                        <div className={`${themeClass}card-list`}>
                            { favArr.map((item)=> (
                                    <CardItem
                                        key={item.id}
                                        image_path={item.image_path}
                                        title={item.title}
                                        price={item.price}
                                        onClick={()=>{dispatch(favListDelete(item.id))}}
                                    />
                                )
                            )}
                        </div>
                    )
                    : (
                        <div className="no-items_box">
                            <h3 className={"no-items_box__text"}>No items added yet</h3>
                        </div>
                    )}
            </div>
        </div>
    )
}

export default FavoritePage;