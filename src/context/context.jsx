import React, {createContext, useState} from "react";
import {useDispatch, useSelector} from "react-redux";
import {favListDelete} from "../store/slices/favorite.js";
import {cartListDelete} from "../store/slices/cartList.js";

export const SaveContext = createContext({});


const BtnProvider = ({children}) => {
    const [savedItems, setSavedItems] = useState([]);
    const [inCart, setInCart] = useState([]);
    const [themeClass, setThemeClass] = useState('');
    const dispatch = useDispatch();

    const toggleTheme = () => {
        setThemeClass(prevTheme => prevTheme === '' ? 're-' : '');
    };

    const saveItem = (id) => {
        const isItemExist = savedItems.some(item => item === id);
        if (isItemExist) {
            dispatch(favListDelete(id));
            setSavedItems((prev) => prev.filter( item => item !== id ))
        } else {
            setSavedItems((prev) => [...prev, id]);
        }
    };
    const toCartItem = (id) => {
        const isItemExist = inCart.some(item => item === id);
        if (isItemExist) {
            dispatch(cartListDelete(id));
            setInCart((prev) => prev.filter( item => item !== id ))
        } else {
            setInCart((prev) => [...prev, id]);
        }
    }

    return (
        <SaveContext.Provider value={{
            saveItem,
            savedItems,
            inCart,
            themeClass,
            toCartItem,
            toggleTheme,

        }}>
            {children}
        </SaveContext.Provider>
    );
}

export {  BtnProvider };