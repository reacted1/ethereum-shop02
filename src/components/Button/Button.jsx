import cn from 'classnames'
import PropTypes from "prop-types";
import React from 'react'

import './Button.scss'

const Button = (props) => {
    const {
        onClick,
        type,
        className,
        children,
        ...restProps

    } = props;
    return (
        <button
            onClick={onClick}
            className={cn('button', className)}
            type={type}
            {...restProps}>
            {children}
        </button>
    )
}

Button.defaultProps = {
    type: "button"
}

Button.propTypes = {
    type: PropTypes.string,
    className: PropTypes.string,
    onClick: PropTypes.func,
    children: PropTypes.any,
    restProps: PropTypes.object,
}
export default Button;