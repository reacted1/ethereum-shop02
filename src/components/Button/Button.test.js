import Button from './Button.jsx'
import { render, screen } from "@testing-library/react";
import React from 'react'

describe("Button button test", ()=> {
    it('have to find text ', () => {
        const button = render(<Button>test</Button>)
        expect(button.getByText(/test/i)).toBeInTheDocument
    });
    test("have to apply className", () => {
        const { container } = render(
            <Button className="test-class">test</Button>
        );
        expect(container.firstChild).toHaveClass("test-class");
    });
    test("have to use onClick function", () => {
        const handleClick = jest.fn();
        render(<Button onClick={handleClick}>test</Button>);
        screen.getByText(/test/i).click();
        expect(handleClick).toHaveBeenCalledTimes(1);
    });
    test("have to correctly process restProps", () => {
        const { container } = render(
            <Button data-test="data-attr">test</Button>
        );
        expect(container.firstChild).toHaveAttribute("data-test", "data-attr");
    });
})