import './Header.scss'
import LogoHero from '../../assets/ethereum.svg?react'
import Card from './icons/card.svg?react'
import Fav from './icons/fav.svg?react'
import PropTypes from "prop-types";
import {useSelector} from "react-redux";
import { Link } from "react-router-dom";
import Button from '../../components/Button/Button.jsx'
import {SaveContext} from "../../context/context.jsx";
import {useContext} from "react";

const Header = () => {
    let dir = window.location.pathname;
    const { toggleTheme } = useContext(SaveContext);
    const favList = useSelector((state)=>state.favorite.favoriteList)
    const cartList = useSelector((state)=>state.cart.cartList);

    return (
        <div className="header">
            <div className="header-content">
                <div className="header-logo">
                    <LogoHero/>
                    <Link to={"/"}>
                        <p>以太坊
                        </p>
                    </Link>
                </div>
                <div className="header-buttons">
                    {dir !==  "/cart" ? <Button onClick={toggleTheme} className={"change-theme-btn"}>Change theme</Button> : null}
                    <Link className={"btn"} to={"/favorite"}>
                        {favList.length > 0 && (<p className={"header-counter"}>{favList.length}</p>)}
                        <Fav />
                    </Link>
                    <Link className={"btn"} to={"/cart"}>
                        {cartList.length > 0 && (<p className={"header-counter"}>{cartList.length}</p>)}
                        <Card/>
                    </Link>
                </div>
            </div>
        </div>
    )
}

Header.propTypes = {
    favorite: PropTypes.array
}

export default Header;