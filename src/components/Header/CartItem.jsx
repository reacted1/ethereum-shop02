import {IMG_URL} from "../../constants/API.js";
import PropTypes from "prop-types";

const CartItem = (props) => {
    const {title, price, image_path} = props
    return (
        <div className="cart">
            <div className="cart-item">
                <div className="cart-img-box">
                    <img src={`${IMG_URL}${image_path}`} alt={title}/>
                </div>
                <div className="cart-box">
                    <p className="cart-box_title">
                        {title}
                    </p>
                    <p className="cart-box_price">
                        {price} ETH
                    </p>
                </div>
            </div>
        </div>
    )
}

CartItem.propTypes = {
    title: PropTypes.string,
    price: PropTypes.string,
    image_path: PropTypes.string
}

export default CartItem;