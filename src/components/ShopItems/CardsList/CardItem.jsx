import React, { useContext } from 'react';
import PropTypes from "prop-types";
import Button from "../../Button/Button.jsx";
import {IMG_URL} from "../../../constants/API.js";
import {SaveContext} from "../../../context/context.jsx";

const CardItem = ({id, image_path, title, price, onClick, onCart}) => {
    const { savedItems, themeClass } = useContext(SaveContext);
    const { inCart, toCartItem } = useContext(SaveContext);
    const isInCart = inCart.includes(id);
    const isSaved = savedItems.includes(id);
    const pathName = window.location.pathname;

    return (
        <div className={`${themeClass}card`}>
            <div className={`${themeClass}cardItem`}>
                <div className={`${themeClass}image-box`}>
                    <img src={`${IMG_URL}${image_path}`} alt={title}/>
                </div>
                <div className={`${themeClass}info-box`}>
                    <p className={`${themeClass}info-box_title`}>
                        {title}
                    </p>
                    <p className={`${themeClass}info-box_price`}>
                        {price} ETH ( ${price*3100} )
                    </p>
                </div>
                {(pathName==="/") ? (
                    <div className={`${themeClass}btn-box`}>
                        <Button className={`${themeClass}btn ${themeClass}btn-item ${themeClass}btn-left`} type="button" onClick={onClick}>
                            {isSaved ? 'Saved' : 'Save'}
                        </Button>
                        <Button className={`${themeClass}btn ${themeClass}btn-item ${themeClass}btn-right`} type="button" onClick={() => {
                                onCart();
                                toCartItem(id)
                            }}>
                            {isInCart ? "Remove Cart" : "Add to Cart"}
                        </Button>
                </div>) : (
                    <div className={`${themeClass}btn-box`}>
                        <Button className={`${themeClass}btn ${themeClass}btn-item ${themeClass}btn-remove`} type="button" onClick={onClick}>Remove from list</Button>
                    </div>
                )}
            </div>
        </div>
    )
}

CardItem.propTypes = {
    image_path: PropTypes.string,
    title: PropTypes.string,
    id: PropTypes.string,
    price: PropTypes.string,
    onClick: PropTypes.func,
    onCart: PropTypes.func,
    restProps: PropTypes.any,
}

export default CardItem;