import {useContext, useEffect, useState} from 'react';
import {IMG_URL} from '../../../constants/API.js'
import CardItem from "./CardItem.jsx";
import ModalCard from "../ModalCard/ModalCard.jsx";
import {useDispatch, useSelector} from 'react-redux'
import {favListAdd} from '../../../store/slices/favorite.js'
import {cartListAdd} from '../../../store/slices/cartList.js'
import {actionFetchNfts} from "../../../store/slices/nfts.js";
import {SaveContext} from "../../../context/context.jsx";


import './CardsList.scss'

// eslint-disable-next-line react/prop-types
const CardsList = () => {
    const { saveItem, themeClass } = useContext(SaveContext);
    const nftItems = useSelector((state) => state.nfts.nftsList)
    const [currentCard, setCurrentCard] = useState({});
    const [isModal, setIsModal] = useState(false);

    const dispatch = useDispatch()

    const handleCurrentDate = (item) => {
        return setCurrentCard(item);
    }
    const handleModal = () => setIsModal(!isModal);

    const findInArr = (id) => {
        const el = nftItems.find((el)=> el.id === id);
        return el;
    }

    useEffect(()=> {
        dispatch(actionFetchNfts());
    }, []);

    return (
    <>
        <div className={`${themeClass}card-list`}>
            { nftItems.map((item)=> (
                    <CardItem
                        key={item.id}
                        id={item.id}
                        image_path={item.image_path}
                        title={item.title}
                        price={item.price}
                        onCart={()=> {dispatch(cartListAdd(findInArr(item.id)))}}
                        onClick={() => {
                            handleCurrentDate(item)
                            handleModal();
                        }}
                    />
                )
            )}
        </div>
        <ModalCard
            title={currentCard.title}
            img={`${IMG_URL}${currentCard.image_path}`}
            price={currentCard.price}
            isOpen={isModal}
            onClose={handleModal}
            onCart={()=> {dispatch(cartListAdd(findInArr(item.id)))}}
            onClick={() => {
                saveItem(currentCard.id);
                dispatch(favListAdd(currentCard))
                handleModal()
            }}
        />
    </>
    )
}

export default CardsList;