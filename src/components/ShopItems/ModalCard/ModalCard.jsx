import './ModalCard.scss'
import {ModalBox, ModalClose, ModalContent, ModalFooter, ModalHeader} from '../../Modal/index.js'
import ModalWrapper from '../../Modal/index.js'
import Button from "../../Button/Button.jsx";

import './ModalCard.scss'

// eslint-disable-next-line react/prop-types
const ModalCard = ({title, img, onClick, onClose, isOpen, price}) => {
    return (
        <>
            <ModalWrapper onClick={onClose} isOpen={isOpen}>
                <ModalBox className="modal-card">
                    <ModalClose onClick={onClose}/>
                    <ModalHeader>
                        <div className="modal-image">
                            <img src={img} alt={title}/>
                        </div>
                    </ModalHeader>
                    <ModalContent>
                        <h4 className="modal-title">{title}</h4>
                        <h4 className="modal-price"> {price} ETH <i>( US$ {price*3100} )</i></h4>
                    </ModalContent>
                    <ModalFooter>
                        <div className="modal-button_wrapper">
                            <Button className="modal-btn" onClick={onClick}>Add to Favorite</Button>
                        </div>
                    </ModalFooter>
                </ModalBox>
            </ModalWrapper>
        </>
    )
}
export default ModalCard;