import './Footer.scss'

const Footer = () => {
    return(
        <div className="footer">
            <img src="../../../public/image/robot.png" alt="robo" className="footer-img"/>
            <div className="footer-content">
                <h2 className="footer-title">An open internet... Oopps, already closed.</h2>
                <p className="footer-text">Today, we gain access to 'free' internet services by giving up control of our personal data. Ethereum services are open by default – you just need a wallet. These are free and easy to set up, controlled by you, and work without any personal info.</p>
                <p className="footer-resources">Used resources: https://ethereum.org/en/</p>
            </div>
        </div>
    )
}

export default Footer;