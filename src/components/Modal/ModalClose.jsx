import PropTypes from "prop-types";

const ModalClose = ({onClick}) => {
    return (
        <button type="button" onClick={onClick} className="modal-close">
            X
        </button>
    );
};

ModalClose.propTypes = {
    onClick: PropTypes.func,
}

export default ModalClose;