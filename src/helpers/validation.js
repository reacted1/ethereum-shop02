import * as yup from 'yup';

const phoneRegExp = /^((\\+[1-9]{1,4}[ \\-]*)|(\\([0-9]{2,3}\\)[ \\-]*)|([0-9]{2,4})[ \\-]*)*?[0-9]{3,4}?[ \\-]*[0-9]{3,4}?$/


export const CheckoutFormSchema = yup.object().shape({
    firstName: yup.string()
        .min(2, 'Too Short!')
        .max(50, 'Too Long!')
        .required('Required'),
    lastName: yup.string()
        .min(2, 'Too Short!')
        .max(50, 'Too Long!')
        .required('Required'),
    age: yup.number()
        .min(15, 'Too young to order')
        .max(120, "Incorrect number")
        .required('Required'),
    phone: yup.string().matches(phoneRegExp, 'Phone number is not valid').required('Required'),
    address: yup.string()
    .min(5, "Too short address")
    .max(200, "Too long")
    .required('Required'),
    comment: yup.string().max(400, "Comment is too long"),
})